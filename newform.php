<?php 

require_once(__DIR__.'/../../config.php');
require_once('edit_form.php');
require_once('lib.php');
require_login();
if (isguestuser()) {
    die();
}

global $DB, $PAGE, $OUTPUT;

$PAGE->set_url(new moodle_url('/local/newform/newform.php'));
$PAGE->set_context(\context_system::instance());
$PAGE->set_title(get_string('page_title','local_newform'));
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('page_header', 'local_newform'));

$message = null;
$type = null;

$newform = new edit_form();
if($newform->is_cancelled()) {
    redirect(new moodle_url('/local/newform/newform.php'), 'You have cancelled your request', null, \core\output\notification::NOTIFY_WARNING);
} else if ($newFormData = $newform->get_data()) {
    $formData = new stdClass();
    $formData->name = $newFormData->name;
    $formData->email = $newFormData->email;
    $formData->phone = $newFormData->phone;
    $formData->address = $newFormData->address;
    $DB->insert_record('local_newform',$formData);
    redirect(new moodle_url('/local/newform/newform.php'), 'User with name <b>"'.$newFormData->name.'"</b> has been added', null, \core\output\notification::NOTIFY_SUCCESS);
}

if(!is_null($message)){
\core\notification::add($message, $type);
}

ob_start();
$newform->display();
$mform = ob_get_clean();

//template part start here

$renderer = $PAGE->get_renderer('local_newform');

echo $OUTPUT->header();

$heading = $renderer->heading();
$table_heading = $renderer->table_heading();

$table_data = outputHtml($renderer);

echo $renderer->start_layouts();

$templatecontext = (object)[
    'heading' => $heading,
    'table_heading' => $table_heading,
    'table' => $table_data,
    'mformHtml'=> $mform,
    'first_pic' => $OUTPUT->image_url('first', 'newform'),
    'second_pic' => $OUTPUT->image_url('img/second', 'newform'),
];


echo $OUTPUT->render_from_template('local_newform/newform',$templatecontext);

echo $renderer->end_layouts();

echo $OUTPUT->footer();

