<?php 

require_once('../../config.php');
 
global $DB, $USER;

$id = required_param('id', PARAM_INT);
$data = $DB->delete_records('local_newform', array('id'=>$id));
if($data){
    redirect('/local/newform/newform.php', 'User has removed from your table', null, \core\output\notification::NOTIFY_WARNING);
}else{
    redirect('/local/newform/newform.php', 'something wents wrong...', null, \core\output\notification::NOTIFY_ERROR);
}
