<?php 

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'New Form';
$string['page_title'] = 'New Form';
$string['page_header'] = 'Profile Data Form';
$string['newform:addinstance'] = 'Add a new newform local plugin';
$string['newform:myaddinstance'] = 'Add a new newform local plugin to my Moodle';
$string['error_name'] = 'Name is required please enter name';
$string['namelabel'] = 'Name';
$string['error_email'] = 'Email address is required please enter email address';
$string['emaillabel'] = 'Email Address';
$string['error_address'] = 'Address is required please enter address';
$string['addresslabel'] = 'Address';
$string['error_phone'] = 'Phone NUmber is required please enter phone number';
$string['phonelabel'] = 'Phone Number';