<?php
defined('MOODLE_INTERNAL') || die();
require_once("{$CFG->libdir}/formslib.php");

class edit_form extends moodleform{

    function definition(){
        $mform =& $this->_form;

        $mform->addElement('text', 'name', get_string('namelabel', 'local_newform'));
        $mform->setType('name', PARAM_RAW);
        $mform->addRule('name', get_string('error_name','local_newform'), 'required', null, 'client');
        // $mform->setDefault('name','Enter your name');

        $mform->addElement('text', 'email', get_string('emaillabel', 'local_newform'));
        $mform->setType('email', PARAM_NOTAGS);
        $mform->addRule('email', get_string('error_email','local_newform'), 'required', null, 'client');
        // $mform->setDefault('email','Enter your email address');


        $mform->addElement('text', 'phone', get_string('phonelabel', 'local_newform'));
        $mform->setType('phone', PARAM_NOTAGS);
        $mform->addRule('phone', get_string('error_phone','local_newform'), 'required', null, 'client');
        // $mform->setDefault('phone','Enter your phone number');

        $mform->addElement('textarea', 'address', get_string('addresslabel', 'local_newform'));
        $mform->setType('address', PARAM_NOTAGS);
        $mform->addRule('address', get_string('error_address','local_newform'), 'required', null, 'client');
        // $mform->setDefault('address','Enter your address');


        $this->add_action_buttons();

    }

}