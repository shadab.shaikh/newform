<?php

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'local_newform';
$plugin->version = 2021030303;
$plugin->requires = 2013011200;