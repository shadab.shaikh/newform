<?php 
defined('MOODLE_INTERNAL') || die();
    
    function outputHtml($renderer){
        global $DB;

        $sql = "SELECT * FROM {local_newform} ORDER BY id DESC";
        $user_list = $DB->get_records_sql($sql);
        $table_data = $renderer->draw_tables($user_list);
        return $table_data;
        
    }
