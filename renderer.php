<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    
}

require_once('lib.php');

class local_newform_renderer extends plugin_renderer_base{

    public function start_layouts(){
        return html_writer::start_tag('div',['class'=>'mform_container']);
    }

    public function end_layouts(){
        return html_writer::end_tag('div');
    }

    public function draw_tables($user_list){
        $table = '';   
            $table .= html_writer::start_tag('table',array('class'=>'lexible table table-striped table-hover reportlog generaltable generalbox table-sm','style'=>'width:100%'));
            $table .= html_writer::start_tag('tr');
            $table .= html_writer::start_tag('th');
             $table .= 'Name';
            $table .= html_writer::end_tag('th');
                
            $table .= html_writer::start_tag('th');
             $table .= 'Email';
            $table .= html_writer::end_tag('th');

            $table .= html_writer::start_tag('th');
                $table .= 'Phone';
            $table .= html_writer::end_tag('th');

            $table .= html_writer::start_tag('th');
                $table .= 'Address'; 
            $table .= html_writer::end_tag('th');

            $table .= html_writer::start_tag('th');
                $table .= 'Action'; 
            $table .= html_writer::end_tag('th');

            $table .= html_writer::end_tag('tr');

            $table .= html_writer::start_tag('tbody');
             if(count($user_list) > 0){
                foreach($user_list as $list){
                    $table .= html_writer::start_tag('tr');
                        $table .= html_writer::start_tag('td');
                            $table .= $list->name;
                        $table .= html_writer::end_tag('td');
                        
                        $table .= html_writer::start_tag('td');
                            $table .= $list->email;
                        $table .= html_writer::end_tag('td');

                        $table .= html_writer::start_tag('td');
                            $table .= $list->phone;
                        $table .= html_writer::end_tag('td');

                        $table .= html_writer::start_tag('td');
                            $table .= $list->address;
                        $table .= html_writer::end_tag('td');

                        $table .= html_writer::start_tag('td');
                            $table .= html_writer::start_tag('a',array('href'=>new moodle_url('/local/newform/delete.php', array('id' => $list->id))));
                                $table .= 'Delete';
                            $table .= html_writer::end_tag('a');
                        $table .= html_writer::end_tag('td');

                    $table .= html_writer::end_tag('tr');
                }
            }else{
                $table .= html_writer::start_tag('td');
                    $table .= 'No data found';
                $table .= html_writer::end_tag('td');
                $table .= html_writer::start_tag('td');
                
                $table .= html_writer::end_tag('td');
                $table .= html_writer::start_tag('td');
                
                $table .= html_writer::end_tag('td');
                $table .= html_writer::start_tag('td');
                
                $table .= html_writer::end_tag('td');
                $table .= html_writer::start_tag('td');
                
                $table .= html_writer::end_tag('td');
            }
                $table .= html_writer::end_tag('tbody');
            $table .= html_writer::end_tag('table');

        return $table;
    }

    public function heading(){
        $html = '';
        $html .= html_writer::start_tag('h4',['class'=>'newform_heading']);
        $html .= 'Fill this form for your better experiences';
        $html .= html_writer::end_tag('h4');
        
        return $html;
    }

    public function table_heading(){
        $html = '';
        $html .= html_writer::start_tag('h4',['class'=>'newform_heading']);
        $html .= 'User data table';
        $html .= html_writer::end_tag('h4');
        
        return $html;
    }


}


